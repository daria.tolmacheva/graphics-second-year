#include <stdio.h>
#include <opencv2/core/core.hpp>
//#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <sstream>

using std::string;


using namespace cv;

cv::vector<double> fitPoly(cv::vector<cv::Point> points, int n)\
{
  //Number of points
  int nPoints = points.size();
  //Vectors for all the points' xs and ys
  cv::vector<float> xValues = cv::vector<float>();
  cv::vector<float> yValues = cv::vector<float>();
  //Split the points into two vectors for x and y values
  for (int i = 0;  i < nPoints;  i++)
  {
    xValues.push_back(points[i].x);
    yValues.push_back(points[i].y);
  }

  //Augmented matrix
  double matrixSystem[n+1][n+2];
  for (int row = 0; row < n+1; row++)
  {
    for (int col = 0; col < n+1; col++)
    {
      matrixSystem[row][col] = 0;
      for (int i = 0; i < nPoints; i++)
        matrixSystem[row][col] += pow(xValues[i], row + col);
    }
    matrixSystem[row][n+1] = 0;
    for (int i = 0; i < nPoints; i++)
      matrixSystem[row][n+1] += pow(xValues[i], row) * yValues[i];

  }

  //Array that holds all the coefficients
  double coeffVec[n+2];

  //Gauss reduction
  for (int i = 0; i <= n-1; i++)
    for (int k=i+1; k <= n; k++)
    {
      double t=matrixSystem[k][i]/matrixSystem[i][i];
      for (int j=0;j<=n+1;j++)
        matrixSystem[k][j]=matrixSystem[k][j]-t*matrixSystem[i][j];
    }

  //Back-substitution
  for (int i=n;i>=0;i--)
  {
    coeffVec[i] = matrixSystem[i][n+1];
    for (int j=0;j<=n+1;j++)
      if (j!=i)
        coeffVec[i] = coeffVec[i]-matrixSystem[i][j]*coeffVec[j];
    coeffVec[i] = coeffVec[i]/matrixSystem[i][i];
  }
  //Construct the cv vector and return it
  cv::vector<double> result = cv::vector<double>();
  for (int i = 0; i < n+1; i++)
    result.push_back(coeffVec[i]);
  return result;
}
//Returns the point for the equation determined
//by a vector of coefficents, at a certain x location
cv::Point pointAtX(cv::vector<double> coeff, double x)
{
  double y = 0;
  for (int i = 0; i < coeff.size(); i++)

  y += pow(x, i) * coeff[i];
  return cv::Point(x, y);
}

// global variables


int main(int argc, char *args[])
{
  // Print  the OpenCV version
  printf("OpenCV version: %d.%d\n", CV_MAJOR_VERSION,
                                    CV_MINOR_VERSION);


  //string imagePath("some/image/path");
  if (argc == 2)
  {
    //filter out vertical lines
    //draw a curve | polynimial regression -> the given funvtion

    Mat greyImage, edges ;
    vector<Vec4i> lines;

    //make it gray
    greyImage = imread(args[1], CV_LOAD_IMAGE_GRAYSCALE);

    // namedWindow("Image window",  CV_WINDOW_NORMAL);
    // imshow("Image window", greyImage);
    // waitKey(0);


    //------------------------------------------------------------------------//
    //-------------------------------horizon0---------------------------------//
    //------------------------------------------------------------------------//
    if ((std::string)args[1] == "HorizonArchive/horizon0.jpg") {

      //smooth
      // double sd = 100.0;
      // GaussianBlur(greyImage, greyImage, Size(15.0, 15.0), sd, sd);

      threshold(greyImage, edges, 70, 100, CV_THRESH_BINARY);
      Canny(edges, edges, 15, 20);
      HoughLinesP(edges, lines, 1, CV_PI/180, 1, 1, 1);

      //vector of points for horizontal lines
      vector<Point> points;

      //remove vertical lines
      for (int i=0; i < lines.size(); i++)
      {
        int x1 = lines[i][0], y1 = lines[i][1], x2 = lines[i][2], y2 = lines[i][3];

        //Check how far are the points in on x axis
        if (abs(x1 - x2) >= 0 || abs(x1 - x2) <= 50)
        {
          Point pt1, pt2;
          pt1.x = x1;
          pt1.y = y1;
          pt2.x = x2;
          pt2.y = y2;
          points.push_back(pt1);
          points.push_back(pt2);
        }//end if
      }//end for
      //perform polynomial regression to find best-fitting curve
      vector<double> coeff;
      coeff = fitPoly(points, 2);

      vector<Point> result;
      for (int i = 0; i < points.size(); i++)
      {
        result.push_back(pointAtX(coeff, points[i].x));
      }//end for

      for (int i = 0; i < result.size(); i++) {
        circle(edges, cvPoint(result[i].x, (result[i].y+1760)), 20, CV_RGB(255, 255, 255));
      } //end for

      namedWindow("Edge image", CV_WINDOW_NORMAL);
      imshow("Edge image", edges);
      namedWindow("Image window",  CV_WINDOW_NORMAL);
      imshow("Image window", greyImage);
      waitKey(0);

      return 0;


      //------------------------------------------------------------------------//
      //-------------------------------horizon1---------------------------------//
      //------------------------------------------------------------------------//
    } else if ((std::string)args[1] == "HorizonArchive/horizon1.jpg") {
      threshold(greyImage, edges, 20, 260, CV_THRESH_BINARY);
      Canny(edges, edges, 10, 100);
      HoughLinesP(edges, lines, 1, CV_PI/180, 1, 1, 1);

      //vector of points for horizontal lines
      vector<Point> points;

      //remove vertical lines
      for (int i=0; i < lines.size(); i++)
      {
        int x1 = lines[i][0], y1 = lines[i][1], x2 = lines[i][2], y2 = lines[i][3];

        //Check how far are the points in on x axis
        if (abs(x1 - x2) >= 0 || abs(x1 - x2) <= 50)
        {
          Point pt1, pt2;
          pt1.x = x1;
          pt1.y = y1;
          pt2.x = x2;
          pt2.y = y2;
          points.push_back(pt1);
          points.push_back(pt2);
        }//end if
      }//end for
      //perform polynomial regression to find best-fitting curve
      vector<double> coeff;
      coeff = fitPoly(points, 2);

      vector<Point> result;
      for (int i = 0; i < points.size(); i++)
      {
        result.push_back(pointAtX(coeff, points[i].x));
      }//end for

      for (int i = 0; i < result.size(); i++) {
        circle(edges, cvPoint(result[i].x, (result[i].y)), 20, CV_RGB(255, 255, 255));
      } //end for

      namedWindow("Edge image", CV_WINDOW_NORMAL);
      imshow("Edge image", edges);
      namedWindow("Image window",  CV_WINDOW_NORMAL);
      imshow("Image window", greyImage);
      waitKey(0);

      return 0;


      //------------------------------------------------------------------------//
      //-------------------------------horizon2---------------------------------//
      //------------------------------------------------------------------------//
    } else if ((std::string)args[1] == "HorizonArchive/horizon2.png") {
      double sd = 200.0;
      GaussianBlur(greyImage, greyImage, Size(15.0, 15.0), sd, sd);
      threshold(greyImage, edges, 2, 100, CV_THRESH_BINARY);
      Canny(edges, edges, 40, 50);
      HoughLinesP(edges, lines, 1, CV_PI/180, 10, 8, 30);

      //vector of points for horizontal lines
      vector<Point> points;

      //remove vertical lines
      for (int i=0; i < lines.size(); i++)
      {
        int x1 = lines[i][0], y1 = lines[i][1], x2 = lines[i][2], y2 = lines[i][3];

        //Check how far are the points in on x axis
        if (abs(x1 - x2) >= 0 || abs(x1 - x2) <= 50)
        {
          Point pt1, pt2;
          pt1.x = x1;
          pt1.y = y1;
          pt2.x = x2;
          pt2.y = y2;
          points.push_back(pt1);
          points.push_back(pt2);
        }//end if
      }//end for
      //perform polynomial regression to find best-fitting curve
      vector<double> coeff;
      coeff = fitPoly(points, 2);

      vector<Point> result;
      for (int i = 0; i < points.size(); i++)
      {
        result.push_back(pointAtX(coeff, points[i].x));
      }//end for

      for (int i = 0; i < result.size(); i++) {
        circle(edges, cvPoint(result[i].x, (result[i].y)), 20, CV_RGB(255, 255, 255));
      } //end for

      namedWindow("Edge image", CV_WINDOW_NORMAL);
      imshow("Edge image", edges);
      namedWindow("Image window",  CV_WINDOW_NORMAL);
      imshow("Image window", greyImage);
      waitKey(0);

      return 0;

      //------------------------------------------------------------------------//
      //-------------------------------horizon3---------------------------------//
      //------------------------------------------------------------------------//
    } else if ((std::string)args[1] == "HorizonArchive/horizon3.jpg") {
      threshold(greyImage, edges, 150, 280, CV_THRESH_BINARY);
      Canny(edges, edges, 40, 50);
      HoughLinesP(edges, lines, 1, CV_PI/180, 5, 24, 2);

      //vector of points for horizontal lines
      vector<Point> points;

      //remove vertical lines
      for (int i=0; i < lines.size(); i++)
      {
        int x1 = lines[i][0], y1 = lines[i][1], x2 = lines[i][2], y2 = lines[i][3];

        //Check how far are the points in on x axis
        if (abs(x1 - x2) >= 0 || abs(x1 - x2) <= 50)
        {
          Point pt1, pt2;
          pt1.x = x1;
          pt1.y = y1;
          pt2.x = x2;
          pt2.y = y2;
          points.push_back(pt1);
          points.push_back(pt2);
        }//end if
      }//end for
      //perform polynomial regression to find best-fitting curve
      vector<double> coeff;
      coeff = fitPoly(points, 2);

      vector<Point> result;
      for (int i = 0; i < points.size(); i++)
      {
        result.push_back(pointAtX(coeff, points[i].x));
      }//end for

      for (int i = 0; i < result.size(); i++) {
        circle(edges, cvPoint(result[i].x, (result[i].y)), 20, CV_RGB(255, 255, 255));
      } //end for

      namedWindow("Edge image", CV_WINDOW_NORMAL);
      imshow("Edge image", edges);
      namedWindow("Image window",  CV_WINDOW_NORMAL);
      imshow("Image window", greyImage);
      waitKey(0);

      return 0;

    } else {
      printf("ignored all ifs\n");
    }//end if

    //waitKey(0);
  }

  return 0;
}
