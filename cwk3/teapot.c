#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "bitmap.c"

/*
This is for COMP27112 Course Assignment 3.

Author: Toby Howard, 5/04/12.
 */

GLfloat xRotation, yRotation= 0.0;
GLint mouseX, mouseY;

GLfloat white_light[] =     { 1.0, 0.86, 0.35, 1.0 };
GLfloat light_position0[] = { 10.0, -10.0, 10.0, 0.0 };

GLfloat extra_light[] =     { 1.0, 0.35, 0.48, 1.0 };  //added
GLfloat light_position1[] = { -7.0, 10.0, 3.0, 0.0 };  //added

GLfloat matSpecular[] =     { 0.1, 0.1, 0.1, 10.0 };
GLfloat matShininess[] =    { 100.0 };
GLfloat matSurface[] =      { 0.3, 1.0, 0.0, 0.5 };

BITMAPINFO *TexInfo; // Texture bitmap information
GLubyte    *TexBits; // Texture bitmap pixel bits

int smooth = 1;


void initialise(void) {
   TexBits = LoadDIBitmap("moss1.bmp", &TexInfo);

   glLightfv(GL_LIGHT0, GL_POSITION, light_position0);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light);
   glLightfv(GL_LIGHT0, GL_SPECULAR, white_light);
    //added
   glLightfv(GL_LIGHT1, GL_POSITION, light_position1);
   glLightfv(GL_LIGHT1, GL_DIFFUSE, extra_light);
   glLightfv(GL_LIGHT1, GL_SPECULAR, extra_light);

   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);
   glEnable(GL_LIGHT1); //added
   glEnable(GL_DEPTH_TEST);
   glShadeModel(GL_SMOOTH);
   //glShadeModel(GL_FLAT);
}

void display(void) {
   glClearColor(0.0, 0.01, 0.01, 1.0); // yellow
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

// define material properties
   glMaterialfv(GL_FRONT, GL_SPECULAR,  matSpecular);
   glMaterialfv(GL_FRONT, GL_SHININESS, matShininess);
   glMaterialfv(GL_FRONT, GL_AMBIENT,   matSurface);


 /*
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, 3, TexInfo->bmiHeader.biWidth,
               TexInfo->bmiHeader.biHeight, 0, GL_BGR_EXT,
               GL_UNSIGNED_BYTE, TexBits);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  glEnable(GL_TEXTURE_2D);
  */

 // draw the teapot
   glPushMatrix();
      glRotatef(xRotation, 1.0, 0.0, 0.0);
      glRotatef(yRotation, 0.0, 1.0, 0.0);
      glutSolidTeapot(0.7);
   glPopMatrix();
   glutSwapBuffers();
}

void reshape(int w, int h) {
   glViewport(0, 0,(GLsizei) w,(GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(17.0, (GLfloat)w/(GLfloat)h, 1.0, 20.0);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   gluLookAt(5.0,5.0,5.0, 0.0,0.0,0.0, 0.0,1.0,0.0);
}


void keyboard(unsigned char key, int x, int y) {
   switch(key) {
	  case 27:  exit(0); break;
    case 's':
      if (smooth == 1)
      {
        glShadeModel(GL_FLAT);
        smooth = 0;
      }
      else
      {
        glShadeModel(GL_SMOOTH);
        smooth = 1;
      }// end if
      break;
   }
   glutPostRedisplay();
}

void mouseMotion(int x, int y) {
// Called when mouse moves
	xRotation+=(y-mouseY);	mouseY= y;
	yRotation+=(x-mouseX);	mouseX= x;
	// keep all rotations between 0 and 360.
	if ( xRotation > 360.0) xRotation-= 360.0;
	if ( xRotation < 0.0)   xRotation+= 360.0;
	if ( yRotation > 360.0) yRotation-= 360.0;
	if ( yRotation < 0.0)   yRotation+= 360.0;
	// ask for redisplay
	glutPostRedisplay();
}


void mousePress(int button, int state, int x, int y) {
// When left mouse button is pressed, save the mouse(x,y)
	if((button == GLUT_LEFT_BUTTON) &&(state == GLUT_DOWN)) {
		mouseX= x;
		mouseY= y;
	}
}

int main(int argc, char** argv) {
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
   glutInitWindowSize(800, 800);
   glutCreateWindow("COMP27112 Coursework Assignment 3: The Teapot");
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutMouseFunc(mousePress);
   glutMotionFunc(mouseMotion);
   initialise();
   glutMainLoop();
   return 0;
}
