#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

int main(int argc, char *argv[])
{
  // Print  the OpenCV version
  printf("OpenCV version: %d.%d\n", CV_MAJOR_VERSION,
                                    CV_MINOR_VERSION);

  //char* imagePath;
  string imagePath("some/image/path");
  if (argc == 2)
  {
    //strcpy(imagePath, argv[1]);
    imagePath = argv[1];
    Mat image = imread(imagePath, CV_LOAD_IMAGE_COLOR);
    namedWindow("Image window",  CV_WINDOW_AUTOSIZE );
    imshow("Image window", image);
    waitKey(0);
  }

  return 0;
}
