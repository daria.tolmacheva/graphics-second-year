#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
 #include<sstream>


using namespace cv;

std::string to_string(double value)
    {
      //create an output string stream
      std::ostringstream os ;

      //throw the value into the string stream
      os << value ;

      //convert the string stream into a string and return
      return os.str() ;
    }

cv::Mat drawHist(cv::Mat hist, int scale)
{
  double mx = 0;
  cv::minMaxLoc(hist, 0, &mx, 0, 0) ;

  cv::Mat  result = cv::Mat::zeros(256*scale, 256*scale, CV_8UC1);

  for(int i = 0; i < 255; i++)
  {
    //Get the histogram value
    float histValue = hist.at<float>(i, 0);
    float nextValue = hist.at<float>(i+1, 0);

    // Create 4 points f o r the poly
    cv::Point p1 = cv::Point(i*scale, 256*scale);
    cv::Point p2 = cv::Point(i*scale + scale, 256*scale);
    cv::Point p3 = cv::Point(i*scale + scale, (256 - nextValue*256/mx)*scale);
    cv::Point p4 = cv::Point(i*scale, (256 - nextValue*256/mx)*scale);

    //Draw the poly ( Ending in p1 )
    int numPoints = 5;
    cv::Point points[] = {p1, p2, p3, p4, p1};
    cv::fillConvexPoly(result, points,numPoints, cv::Scalar::all(256), 0, 0);
  }
  return result;
}

// global variables
Mat greyImage, blurredImage, diffImage, hist;
int* blurVariable = (int*)malloc(sizeof(int));

void on_change(int , void* )
{
  //get standart deviation from the slider value
  double sd = ((double)*blurVariable/10.0);
  //apply gaussian smoothing
  GaussianBlur(greyImage, blurredImage, Size(9.0, 9.0), sd, sd);
  // show the image
  imshow("Image window", blurredImage);

  //difference
  diffImage = greyImage - blurredImage + 128;

  std::string text = "SD = ";
  text += to_string(sd);
  text += ", ksize = 9x9";

  putText(diffImage, text, cvPoint(10, 50), FONT_HERSHEY_COMPLEX_SMALL,
          1, cvScalar(1, 1, 1), 1);


  imshow("Difference window", diffImage);

  // int hbins = 32, sbins = 30;
  // int histSize[] = {hbins, sbins};
  // // hue varies from 0 to 179, see cvtColor
  // float hranges[] = {0, 180};
  // // saturation varies from 0 (black-gray-white) to
  // // 255 (pure spectrum color)
  // float sranges[] = {0, 256};
  // const float* ranges[] = { hranges, sranges };
  // // we compute the histogram from the 0-th and 1-st channels
  // int channels[] = {0, 1};
  // //Mat diffImageArray[1] = {diffImage};
  // //Mat histArray[1] = {&hist};
  // calcHist(&diffImage, 1, channels, Mat(), hist, 2, histSize, ranges);

  // Mat histDraw = drawHist(hist, 100);
  //
  // std::string text = "SD = ";
  // text += to_string(sd);
  // text += ", ksize = 9x9";
  //
  // putText(histDraw, text, cvPoint(10, 50), FONT_HERSHEY_COMPLEX_SMALL,
  //         1, cvScalar(1, 1, 1), 1);
  // imshow("Histogram window", histDraw);


} //end on change

int main(int argc, char *argv[])
{
  // Print  the OpenCV version
  printf("OpenCV version: %d.%d\n", CV_MAJOR_VERSION,
                                    CV_MINOR_VERSION);


  string imagePath("some/image/path");
  if (argc == 2)
  {

    imagePath = argv[1];
    Mat image = imread(imagePath, CV_LOAD_IMAGE_COLOR);
    cvtColor(image, greyImage, CV_RGB2GRAY);
    namedWindow("Image window",  CV_WINDOW_AUTOSIZE);
    namedWindow("Difference window",  CV_WINDOW_AUTOSIZE);
    namedWindow("Histogram window",  CV_WINDOW_AUTOSIZE);

    imshow("Image window", greyImage);
    *blurVariable = 1;
    createTrackbar("Blur Intencity (out of 50)", "Image window", blurVariable, 50,
                    on_change);
    waitKey(0);
  }

  return 0;
}
