@RequestMapping(value = "/{id}/next3events",method = RequestMethod.GET)
	public Resources<Resource<Event>> getNext3EventsInVenue(@PathVariable("id") long id) throws VenueNotFoundException {

	try {
		Venue venue = venueService.findOne(id);
		List<Event> nearestThree = new ArrayList<Event>();
		if (!venue.getFutureEvents().isEmpty()){
			List<Event> futureEvents = venue.getFutureEvents();

			if (futureEvents.size() > 3){
				nearestThree = futureEvents.subList(0, 3);
			} else {
				nearestThree = futureEvents;
			}
		}
		return eventToResource(nearestThree);
	} catch (Exception e) {
		throw new VenueNotFoundException("Id: " + id);
	}
	}


	private Resource<Event> eventToResource(Event event) {
		Link selfLink = linkTo(EventsControllerApi.class).slash(event.getId()).withSelfRel();
		Link venueLink = linkTo(EventsControllerApi.class).slash(event.getId()).withRel("venue");

		return new Resource<Event>(event, selfLink, venueLink);
	}

	private Resources<Resource<Event>> eventToResource(Iterable<Event> events) {
		Link selfLink = linkTo(methodOn(EventsControllerApi.class).getAllEvents()).withSelfRel();

		List<Resource<Event>> resources = new ArrayList<Resource<Event>>();
		for (Event event : events) {
			resources.add(eventToResource(event));
		}

		return new Resources<Resource<Event>>(resources, selfLink);
	}
	
