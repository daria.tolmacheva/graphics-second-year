#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include<sstream>

using namespace cv;

// global variables
Mat greyImage, bnwimage;
int* theta = (int*)malloc(sizeof(int));

void on_change(int , void* )
{
  double doubleTheta = ((double)*theta/1.0);
  threshold(greyImage, bnwimage, doubleTheta, 255.0, CV_THRESH_BINARY);
  imshow("Image window", bnwimage);
} //end on change


int main(int argc, char *argv[])
{
  // Print  the OpenCV version
  printf("OpenCV version: %d.%d\n", CV_MAJOR_VERSION,
                                    CV_MINOR_VERSION);


  //string imagePath("some/image/path");
  if (argc == 2)
  {
    Mat image = imread(argv[1], 1);
    //Mat image = imread(argv[1], CV_LOAD_IMAGE_COLOR);
    cvtColor(image, greyImage, CV_RGB2GRAY);
    //Mat greyImage = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);


    namedWindow("Image window",  CV_WINDOW_NORMAL);

    imshow("Image window", greyImage);
    *theta = 1.0;
    createTrackbar("Theta value", "Image window", theta, 255,
                    on_change);
    waitKey(0);
  }

  return 0;
}
