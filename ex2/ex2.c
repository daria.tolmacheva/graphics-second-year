/*
==========================================================================
File:        ex2.c (skeleton)
Version:     5, 19/12/2017
Author:     Toby Howard and Daria Tolmacheva
==========================================================================
*/

/* The following ratios are not to scale: */
/* Moon orbit : planet orbit */
/* Orbit radius : body radius */
/* Sun radius : planet radius */

#ifdef MACOS
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_BODIES 25
#define TOP_VIEW 1
#define ECLIPTIC_VIEW 2
#define SHIP_VIEW 3
#define EARTH_VIEW 4
#define PI 3.141593
#define DEG_TO_RAD 0.01745329
#define ORBIT_POLY_SIDES 50
#define TIME_STEP 0.5   /* days per frame */

typedef struct {
  char    name[25];       /* name */
  GLfloat r,g,b;          /* colour */
  GLfloat orbital_radius; /* distance to parent body (km) */
  GLfloat orbital_tilt;   /* angle of orbit wrt ecliptic (deg) */
  GLfloat orbital_period; /* time taken to orbit (days) */
  GLfloat radius;         /* radius of body (km) */
  GLfloat axis_tilt;      /* tilt of axis wrt body's orbital plane (deg) */
  GLfloat rot_period;     /* body's period of rotation (days) */
  GLint   orbits_body;    /* identifier of parent body */
  GLfloat spin;           /* current spin value (deg) */
  GLfloat orbit;          /* current orbit value (deg) */
 } body;

body  bodies[MAX_BODIES];
int   numBodies, current_view, draw_orbits, draw_labels, draw_starfield;
float  date;

GLint slises = 20;
GLint stacks = 20;
int worldAxisOn = 1;
GLfloat starX[1000], starY[1000], starZ[1000];
int firstCall = 1;

/*****************************/

float myRand (void)
{
  /* return a random float in the range [0,1] */

  return (float) rand() / RAND_MAX;
}

/*****************************/

void drawStarfield (void)
{
  /* This is for you to complete. */
  int i;
  if (firstCall == 1){
    for (i = 0; i < 1000; i++)
    {
      starX[i] = (myRand() - myRand()) * 600000000.0;
      starY[i] = (myRand() - myRand()) * 600000000.0;
      starZ[i] = (myRand() - myRand()) * 600000000.0;
    }//end for
    firstCall = 0;
  }//enf if

  for (i = 0; i < 1000; i++)
  {
    glColor3f(1.0,1.0,1.0);
    glBegin(GL_POINTS);
    glVertex3f(starX[i], starY[i], starZ[i]);
    glEnd();
  }//end for
}

/*****************************/

void readSystem(void)
{
  /* reads in the description of the solar system */

  FILE *f;
  int i;

  f= fopen("sys", "r");
  if (f == NULL) {
     printf("ex2.c: Couldn't open 'sys'\n");
     printf("To get this file, use the following command:\n");
     printf("  cp /opt/info/courses/COMP27112/ex2/sys .\n");
     exit(0);
  }
  fscanf(f, "%d", &numBodies);
  for (i= 0; i < numBodies; i++)
  {
    fscanf(f, "%s %f %f %f %f %f %f %f %f %f %d",
      bodies[i].name,
      &bodies[i].r, &bodies[i].g, &bodies[i].b,
      &bodies[i].orbital_radius,
      &bodies[i].orbital_tilt,
      &bodies[i].orbital_period,
      &bodies[i].radius,
      &bodies[i].axis_tilt,
      &bodies[i].rot_period,
      &bodies[i].orbits_body);

    /* Initialise the body's state */
    bodies[i].spin= 0.0;
    bodies[i].orbit= myRand() * 360.0; /* Start each body's orbit at a
                                          random angle */
    bodies[i].radius*= 1000.0; /* Magnify the radii to make them visible */
  }
  fclose(f);
}

/*****************************/

void drawString (void *font, float x, float y, char *str)
{ /* Displays the string "str" at (x,y,0), using font "font" */

  /* This is for you to complete. */

}

/*****************************/

void setView (void) {
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  switch (current_view) {
  case TOP_VIEW:
    gluLookAt(0.0, 0.0, 600000000.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0);
    break;
  case ECLIPTIC_VIEW:
    gluLookAt(0.0, -600000000.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
    break;
  case SHIP_VIEW:
    gluLookAt(-600000000.0, 0.0, 227940000.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
    break;
  case EARTH_VIEW:

    break;
  }
}

/*****************************/

void menu (int menuentry) {
  switch (menuentry) {
  case 1: current_view= TOP_VIEW;
          break;
  case 2: current_view= ECLIPTIC_VIEW;
          break;
  case 3: current_view= SHIP_VIEW;
          break;
  case 4: current_view= EARTH_VIEW;
          break;
  case 5: draw_labels= !draw_labels;
          break;
  case 6: draw_orbits= !draw_orbits;
          break;
  case 7: draw_starfield= !draw_starfield;
          break;
  case 8: exit(0);
  }
}

/*****************************/

void init(void)
{
  /* Define background colour */
  glClearColor(0.0, 0.0, 0.0, 0.0);

  glutCreateMenu (menu);
  glutAddMenuEntry ("Top view", 1);
  glutAddMenuEntry ("Ecliptic view", 2);
  glutAddMenuEntry ("Spaceship view", 3);
  glutAddMenuEntry ("Earth view", 4);
  glutAddMenuEntry ("", 999);
  glutAddMenuEntry ("Toggle labels", 5);
  glutAddMenuEntry ("Toggle orbits", 6);
  glutAddMenuEntry ("Toggle starfield", 7);
  glutAddMenuEntry ("", 999);
  glutAddMenuEntry ("Quit", 8);
  glutAttachMenu (GLUT_RIGHT_BUTTON);

  current_view= TOP_VIEW;
  draw_labels= 1;
  draw_orbits= 1;
  draw_starfield= 1;
}

/*****************************/

void animate(void)
{
  int i;

    date+= TIME_STEP;

    for (i= 0; i < numBodies; i++)  {
      bodies[i].spin += 360.0 * TIME_STEP / bodies[i].rot_period;
      bodies[i].orbit += 360.0 * TIME_STEP / bodies[i].orbital_period;
      glutPostRedisplay();
    }
}

/*****************************/

void drawOrbit (int n)
{ /* Draws a polygon to approximate the circular
     orbit of body "n" */
    int i;
    float angle, x, y, z;
    glBegin(GL_LINE_LOOP);
      for (i = 0; i < ORBIT_POLY_SIDES; i++){
        angle = 360.0/ORBIT_POLY_SIDES * i;
        x = bodies[n].orbital_radius * cos(angle * DEG_TO_RAD);
        y = bodies[n].orbital_radius * sin(angle * DEG_TO_RAD);
        z = 0;
        y = y * cos(bodies[n].orbital_tilt * DEG_TO_RAD) - z * sin(bodies[n].orbital_tilt * DEG_TO_RAD);
        z = y * sin(bodies[n].orbital_tilt * DEG_TO_RAD) + z * cos(bodies[n].orbital_tilt * DEG_TO_RAD);
        glVertex3f(x, y, z);
      }//end for
    glEnd();

}

/*****************************/

void drawLabel(int n)
{ /* Draws the name of body "n" */

    /* This is for you to complete. */
}

/*****************************/

void drawBody(int n)
{
 /* Draws body "n" */



 int i;
 int parent;
 glMatrixMode(GL_MODELVIEW);





 //sun to test view
 glColor3f(bodies[0].r, bodies[0].b, bodies[0].g);
 glPushMatrix();
   glRotatef(bodies[0].spin, 0.0, 0.0, 1.0);
   glRotatef(bodies[0].axis_tilt, 0.0, 1.0, 0.0);
   glBegin(GL_LINES);
     glVertex3f(0.0, 0.0, -2 * bodies[0].radius);
     glVertex3f(0.0, 0.0, 2 * bodies[0].radius);
   glEnd();
   glutWireSphere(bodies[0].radius, slises, stacks);

 glPopMatrix();

 for (i = 1; i < 5; i++){
   glColor3f(bodies[i].r, bodies[i].b, bodies[i].g);
   glPushMatrix();
     glRotatef(bodies[i].orbital_tilt, 1.0, 0.0, 0.0);
     glRotatef(bodies[i].orbit, 0.0, 0.0, 1.0);
     glTranslatef(bodies[i].orbital_radius, 0.0, 0.0);

     glRotatef(0.0 - bodies[i].orbital_tilt, 1.0, 0.0, 0.0);
     glRotatef(0.0 - bodies[i].orbit, 0.0, 0.0, 1.0);

     glRotatef(bodies[i].axis_tilt, 0.0, 1.0, 0.0);

     glRotatef(bodies[i].spin, 0.0, 0.0, 1.0);
     glBegin(GL_LINES);
       glVertex3f(0.0, 0.0, -2 * bodies[i].radius);
       glVertex3f(0.0, 0.0, 2 * bodies[i].radius);
     glEnd();
     glutWireSphere(bodies[i].radius, slises, stacks);
   glPopMatrix();
   drawOrbit(i);
 }//end for
 for (i = 5; i < 8; i++){
   glColor3f(bodies[i].r, bodies[i].b, bodies[i].g);
   glPushMatrix();
     parent = bodies[i].orbits_body;
     glRotatef(bodies[parent].orbital_tilt, 1.0, 0.0, 0.0);
     glRotatef(bodies[parent].orbit, 0.0, 0.0, 1.0);
     glTranslatef(bodies[parent].orbital_radius, 0.0, 0.0);
     glRotatef(bodies[parent].axis_tilt, 0.0, 1.0, 0.0);

     glRotatef(bodies[i].orbital_tilt, 1.0, 0.0, 0.0);
     glRotatef(bodies[i].orbit, 0.0, 0.0, 1.0);
     glTranslatef(bodies[i].orbital_radius, 0.0, 0.0);
     glRotatef(bodies[i].axis_tilt, 0.0, 1.0, 0.0);
     glRotatef(bodies[i].spin, 0.0, 0.0, 1.0);
     glBegin(GL_LINES);
       glVertex3f(0.0, 0.0, -2 * bodies[i].radius);
       glVertex3f(0.0, 0.0, 2 * bodies[i].radius);
     glEnd();
     glutWireSphere(bodies[i].radius, slises, stacks);

   glPopMatrix();
   //drawOrbit(i);
 }//end for
}

/*****************************/

void drawWorldAxis(void)
{
   /* Draws world axis" */

  if (worldAxisOn == 1) {
   // x axis
   float LEN= 20000.0;

   glLineWidth(5.0);

   glBegin(GL_LINES);
   glColor3f(1.0,0.0,0.0); // red
       glVertex3f(0.0, 0.0, 0.0);
       glVertex3f(LEN, 0.0, 0.0);

   glColor3f(0.0,1.0,0.0); // green
       glVertex3f(0.0, 0.0, 0.0);
       glVertex3f(0.0, LEN, 0.0);

   glColor3f(0.0,0.0,1.0); // blue
       glVertex3f(0.0, 0.0, 0.0);
       glVertex3f(0.0, 0.0, LEN);
   glEnd();

   glLineWidth(1.0);
 }//end if

}

/*****************************/

void display(void)
{
  int i;
  glClear(GL_COLOR_BUFFER_BIT);
  /* set the camera */
  setView();

  //drawWorldAxis();

  if (draw_starfield) drawStarfield();

  drawWorldAxis();


  for (i= 0; i < numBodies; i++)
  {
    glPushMatrix();
      drawBody (i);
    glPopMatrix();
  }

  //drawWorldAxis();

  glLineWidth(1.0f);
   glColor3f(1.0, 0.0, 0.0);
   glBegin(GL_LINES);
     glVertex3f(0.0, 0.0, -40000000000.0);
     glVertex3f(0.0, 0.0, 40000000000.0);
     glColor3f(0.0, 1.0, 0.0);
     glVertex3f(-40000000000.0, 0.0, 0.0);
     glVertex3f(40000000000.0, 0.0, 0.0);
     glColor3f(0.0, 0.0, 1.0);
     glVertex3f(0.0, -40000000000.0, 0.0);
     glVertex3f(0.0, 40000000000.0, 0.0);
   glEnd();

  glutSwapBuffers();
}

/*****************************/

void reshape(int w, int h)
{
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective (48.0, (GLfloat)w/(GLfloat)h,  1000.0, 8000000000.0);
}

/*****************************/

void keyboard(unsigned char key, int x, int y)
{
  switch(key)
  {
    case 27:  /* Escape key */
      exit(0);
    case 'a':
      if (worldAxisOn == 1){
        worldAxisOn = 0;
      } else {
        worldAxisOn = 1;
      }//end if
  }
}

/*****************************/

int main(int argc, char** argv)
{
  glutInit (&argc, argv);
  glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
  glutCreateWindow ("COMP27112 Exercise 2");
  //glutFullScreen();
  init();
  glutDisplayFunc (display);
  glutReshapeFunc (reshape);
  glutKeyboardFunc (keyboard);
  glutIdleFunc (animate);
  readSystem();
  glutMainLoop();
  return 0;
}
/* end of ex2.c */
